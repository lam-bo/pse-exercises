package com.lamboapps.lawnmower.core.model


data class Position(val x: Int, val y: Int) {

    fun forwardInGrid(forward: (Position) -> Position, maxGridPosition: Position): Position {
        val gridMax = maxGridPosition.takeIf { it.x >= 0 && it.y >= 0 }
        return forward(this).let {
            copy(x = it.x.coerceIn(0, gridMax?.x), y = it.y.coerceIn(0, gridMax?.y))
        }
    }
}
