package com.lamboapps.lawnmower.core.model

data class RawInstruction(val locationLine: String, val moveLine: String?) {
    companion object Adapter {

        private val moveMap = mapOf(
            'G' to MovesProgram.Move.TURN_LEFT,
            'D' to MovesProgram.Move.TURN_RIGHT,
            'A' to MovesProgram.Move.FORWARD
        )

        private val orientationMap = mapOf('N' to North, 'E' to East, 'S' to South, 'W' to West)

        private fun <K, V> Map<K, V>.reversed() = HashMap<V, K>().also { newMap ->
            entries.forEach { newMap[it.value] = it.key }
        }

        fun rawInstructionsFromString(rawData: String): List<RawInstruction>? {
            val gridInstruction = rawData.lineSequence().firstOrNull()?.let { RawInstruction(it, null) }
                ?: return null
            val programInstructionSeq = rawData.lineSequence().drop(1).windowed(2, 2, false)
                .map { programLines -> RawInstruction(programLines[0], programLines[1]) }
            return listOf(gridInstruction).plus(programInstructionSeq)
        }

        fun rawLocation(program: MovesProgram): String {
            return "${program.position.x} ${program.position.y} ${orientationMap.reversed()[program.orientation]}"
        }
    }

    fun position(): Position? {
        return Pair(locationLine.elementAtOrNull(0), locationLine.elementAtOrNull(2))
            .takeIf { (x, y) -> (x?.isDigit() ?: false && y?.isDigit() ?: false) }
            ?.let { (x, y) -> Position(Character.getNumericValue(x!!), Character.getNumericValue(y!!)) }
            ?.takeIf { position -> (position.x >= 0 && position.y >= 0) }
    }

    fun program() : Triple<Position, Orientation, Iterable<MovesProgram.Move>>? {
        val position = position()
        val orientation = locationLine.elementAtOrNull(4)?.let { orientationMap[it] }
        val moves = moveLine?.toCharArray()?.map { moveMap[it] }?.filterNotNull()
        return if(position != null && orientation != null && moves != null) {
            Triple(position, orientation, moves)
        } else null
    }
}