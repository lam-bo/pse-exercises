package com.lamboapps.lawnmower.core.model

import com.lamboapps.lawnmower.core.model.MovesProgram.Companion.recursiveExecution


class ExecutionState(val programList: Iterable<MovesProgram>) {
    companion object {
        tailrec fun recursiveProgramExecution(current: ExecutionState): ExecutionState {
            return when (val next = current.executeNextProgram()) {
                null -> current
                else -> recursiveProgramExecution(next)
            }
        }
    }

    fun executeNextProgram(): ExecutionState? {
        return programList.firstOrNull { it.hasMovesLeft() }?.let { program ->
            val occupiedPositions = programList.minus(program).map { it.position }
            val programExecuted = recursiveExecution(program, occupiedPositions)
            ExecutionState(programList.map { old -> if (old == program) programExecuted else old })
        }
    }
}
