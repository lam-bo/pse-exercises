package com.lamboapps.lawnmower.core

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.lamboapps.lawnmower.core.model.ExecutionState
import com.lamboapps.lawnmower.core.model.ExecutionState.Companion.recursiveProgramExecution
import com.lamboapps.lawnmower.core.model.MovesProgram
import com.lamboapps.lawnmower.core.model.RawInstruction

class InstructionsInteractor(private val adapter: RawInstruction.Adapter = RawInstruction.Adapter) {

    fun getRawFinalLocations(rawData: String): Either<String, String> {
        adapter.rawInstructionsFromString(rawData)
            .let { rawInstructionList ->
                val gridMaxPosition = rawInstructionList?.firstOrNull()?.position()
                    ?: return Left("grid position not parsable")
                val programList = rawInstructionList.drop(1).mapNotNull { it.program() }
                    .map { (initialPosition, initialOrientation, moves) ->
                        MovesProgram(initialPosition, initialOrientation, gridMaxPosition, moves)
                    }.takeIf { finalList -> finalList.size == rawInstructionList.drop(1).size }
                    ?: return Left("one or more programs is not parsable")
                val finalState = recursiveProgramExecution(ExecutionState(programList))
                finalState.programList.joinToString("\n") { adapter.rawLocation(it) }
            }.apply {
                return Right(this)
            }
    }
}
