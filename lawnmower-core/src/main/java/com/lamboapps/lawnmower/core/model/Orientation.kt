package com.lamboapps.lawnmower.core.model


sealed class Orientation {
    abstract val turnLeft: Orientation
    abstract val turnRight: Orientation
    abstract val nextForward: (Position) -> Position
}

object North : Orientation() {
    override val turnLeft = West
    override val turnRight = East
    override val nextForward: (Position) -> Position = { it.copy(y = it.y + 1) }
}

object East : Orientation() {
    override val turnLeft = North
    override val turnRight = South
    override val nextForward: (Position) -> Position = { it.copy(x = it.x + 1) }
}

object South : Orientation() {
    override val turnLeft = East
    override val turnRight = West
    override val nextForward: (Position) -> Position = { it.copy(y = it.y - 1) }
}


object West : Orientation() {
    override val turnLeft = South
    override val turnRight = North
    override val nextForward: (Position) -> Position = { it.copy(x = it.x - 1) }
}