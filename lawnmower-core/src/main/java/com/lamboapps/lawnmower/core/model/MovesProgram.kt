package com.lamboapps.lawnmower.core.model


data class MovesProgram(
    val position: Position,
    val orientation: Orientation,
    val gridMaxPosition: Position,
    val movesLeft: Iterable<Move>
) {
    companion object {
        tailrec fun recursiveExecution(
            current: MovesProgram,
            occupiedPositions: Iterable<Position>
        ): MovesProgram {
            return when (val next = current.executeNextMove(occupiedPositions)) {
                null -> current
                else -> recursiveExecution(next, occupiedPositions)
            }
        }
    }

    enum class Move { TURN_RIGHT, TURN_LEFT, FORWARD }

    fun hasMovesLeft(): Boolean = movesLeft.firstOrNull() != null

    fun executeNextMove(occupiedPositions: Iterable<Position>): MovesProgram? {
        return movesLeft.firstOrNull()?.let { move ->
            when (move) {
                Move.TURN_RIGHT -> {
                    val nextOrientation = orientation.turnRight
                    copy(movesLeft = movesLeft.drop(1), orientation = nextOrientation)
                }
                Move.TURN_LEFT -> {
                    val nextOrientation = orientation.turnLeft
                    copy(movesLeft = movesLeft.drop(1), orientation = nextOrientation)
                }
                Move.FORWARD -> {
                    val nextPosition = position
                        .forwardInGrid(orientation.nextForward, gridMaxPosition)
                        .takeUnless { occupiedPositions.contains(it) } ?: position
                    copy(movesLeft = movesLeft.drop(1), position = nextPosition)
                }
            }
        }
    }
}
