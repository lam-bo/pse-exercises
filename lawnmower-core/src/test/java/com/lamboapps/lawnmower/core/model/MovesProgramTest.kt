package com.lamboapps.lawnmower.core.model

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.assertEquals
import org.junit.Test

class MovesProgramTest {


    @Test
    fun recursiveExecution_shouldCallExecuteNextMove() {
        val moveProgram : MovesProgram = mock {
            on(it.executeNextMove(any())).thenReturn(null)
        }

        MovesProgram.recursiveExecution(moveProgram, mock())

        verify(moveProgram).executeNextMove(any())
    }

    @Test
    fun hasMovesLeft_shouldReturnTrue_whenMovesNotEmpty() {
        val movesProgram = MovesProgram(mock(), mock(), mock(), listOf(mock()))

        assertEquals(true, movesProgram.hasMovesLeft())
    }

    @Test
    fun executeNextMove_shouldReturnNull_whenNoMovesLeft() {
        val movesProgram = MovesProgram(mock(), mock(), mock(), emptyList())

        assertEquals(null, movesProgram.executeNextMove(mock()))
    }


    @Test
    fun executeNextMove_shouldDoRightTurn_whenNextMoveRightTurn() {
        val movesProgram = MovesProgram(mock(), North, mock(), listOf(MovesProgram.Move.TURN_RIGHT))

        val next = movesProgram.executeNextMove(emptyList())

        assertEquals(East, next?.orientation)
        assertEquals(false, next?.hasMovesLeft())
    }

    @Test
    fun executeNextMove_shouldDoLeftTurn_whenNextMoveLeftTurn() {
        val movesProgram = MovesProgram(mock(), North, mock(), listOf(MovesProgram.Move.TURN_LEFT))

        val next = movesProgram.executeNextMove(emptyList())

        assertEquals(West, next?.orientation)
        assertEquals(false, next?.hasMovesLeft())
    }

    @Test
    fun executeNextMove_shouldMoveForward_whenNextMoveForward() {
        val movesProgram = MovesProgram(Position(1, 1), North, Position(2, 2), listOf(MovesProgram.Move.FORWARD))

        val next = movesProgram.executeNextMove(emptyList())

        assertEquals(Position(1,2), next?.position)
        assertEquals(North, next?.orientation)
        assertEquals(false, next?.hasMovesLeft())
    }

    @Test
    fun executeNextMove_shouldNotMove_whenForwardOnOccupiedPosition() {
        val movesProgram = MovesProgram(Position(1, 1), North, Position(2, 2), listOf(MovesProgram.Move.FORWARD))

        val next = movesProgram.executeNextMove(listOf(Position(1,2)))

        assertEquals(Position(1,1), next?.position)
        assertEquals(North, next?.orientation)
        assertEquals(false, next?.hasMovesLeft())
    }
}