package com.lamboapps.lawnmower.core.model

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test

class ExecutionStateTest {

    @Test
    fun recursiveProgramExecution_shouldCallExecuteNextProgram() {
        val moveProgram : ExecutionState = mock {
            on(it.executeNextProgram()).thenReturn(null)
        }

        ExecutionState.recursiveProgramExecution(moveProgram)

        verify(moveProgram).executeNextProgram()
    }



}