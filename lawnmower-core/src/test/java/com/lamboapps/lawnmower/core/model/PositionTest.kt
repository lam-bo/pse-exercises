package com.lamboapps.lawnmower.core.model

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.assertEquals
import org.junit.Test

class PositionTest {

    @Test
    fun forwardInGrid_shouldInvokeForwardArg() {
        val position = Position(0, 0)
        val forward: (Position) -> Position = mock {
            on(it.invoke(any())).thenReturn(position)
        }

        position.forwardInGrid(forward, Position(0,0))

        verify(forward).invoke(position)
    }

    @Test
    fun forwardInGrid_shouldCoerceInGrid() {
        val position = Position(0, 0)
        val forward: (Position) -> Position = mock {
            on(it.invoke(any())).thenReturn(Position(99999,99999))
        }

        val result = position.forwardInGrid(forward, Position(1,1))

        verify(forward).invoke(position)
        assertEquals(Position(1, 1), result)
    }
}