package com.lamboapps.lawnmower.core.model

import org.junit.Assert.assertEquals
import org.junit.Test

class RawInstructionTest {

    @Test
    fun position_shouldReturnResult() {
        val instruction = RawInstruction("1 2", null)

        assertEquals(Position(1, 2), instruction.position())
    }

    @Test
    fun position_shouldReturnNull_whenMissingOneCoordinate() {
        val instruction = RawInstruction("1", null)

        assertEquals(null, instruction.position())
    }

    @Test
    fun position_shouldReturnNull_whenNotDigits() {
        val instruction = RawInstruction("A B", null)

        assertEquals(null, instruction.position())
    }

    @Test
    fun program_shouldReturnResult() {
        val instruction = RawInstruction("1 2 N", "GADGAD")

        assertEquals(Position(1, 2), instruction.program()!!.first)
        assertEquals(North, instruction.program()!!.second)
        assertEquals(6, instruction.program()!!.third.toList().size)
    }

    @Test
    fun program_shouldReturnNull_whenLocationMissing() {
        val instruction = RawInstruction("", "GADGAD")

        assertEquals(null, instruction.program())
    }

    @Test
    fun program_shouldReturnNull_whenLocationIncorrect() {
        val instruction = RawInstruction("1 2 X", "GADGAD")

        assertEquals(null, instruction.program())
    }

    @Test
    fun program_shouldReturnNull_whenMovesMissing() {
        val instruction = RawInstruction("1 2 N", null)

        assertEquals(null, instruction.program())
    }

    @Test
    fun program_shouldReturnEmptyMoves_whenMovesIncorrect() {
        val instruction = RawInstruction("1 2 N", "X")

        assertEquals(Position(1, 2), instruction.program()!!.first)
        assertEquals(North, instruction.program()!!.second)
        assertEquals(0, instruction.program()!!.third.toList().size)
    }
}