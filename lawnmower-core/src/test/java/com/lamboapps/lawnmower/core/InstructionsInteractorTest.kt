package com.lamboapps.lawnmower.core

import arrow.core.Right
import com.lamboapps.lawnmower.core.model.Position
import com.lamboapps.lawnmower.core.model.RawInstruction
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class InstructionsInteractorTest {

    @Test
    fun getFinalRawLocations_shouldReturnExpected_whenDefaultRawDataResource() {
        val adapter = RawInstruction.Adapter
        val interactor = InstructionsInteractor(adapter)
        val rawData = this::class.java.getResource("/default-raw-data.txt").readText(Charsets.UTF_8)

        val result = interactor.getRawFinalLocations(rawData)

        val expected = Right("1 3 N" + "\n" + "5 1 E")
        assertEquals(expected, result)
    }

    @Test
    fun getFinalRawLocations_shouldReturnError_whenGridPositionIsNotParsable() {
        val rawInstruction: RawInstruction = mock {
            on(it.position()).thenReturn(null)
        }
        val adapter: RawInstruction.Adapter = mock {
            on(it.rawInstructionsFromString(any())).thenReturn(listOf(rawInstruction))
        }
        val interactor = InstructionsInteractor(adapter)

        val result = interactor.getRawFinalLocations("rawData")

        assertEquals(true, result.isLeft())
    }

    @Test
    fun getFinalRawLocations_shouldReturnError_whenAMoveProgramIsNotParsable() {
        val rawInstruction: RawInstruction = mock {
            on(it.position()).thenReturn(Position(1, 1))
            on(it.program()).thenReturn(null)
        }
        val adapter: RawInstruction.Adapter = mock {
            on(it.rawInstructionsFromString(any())).thenReturn((1..5).map { rawInstruction })
        }
        val interactor = InstructionsInteractor(adapter)

        val result = interactor.getRawFinalLocations("rawData")

        assertEquals(true, result.isLeft())
    }
}