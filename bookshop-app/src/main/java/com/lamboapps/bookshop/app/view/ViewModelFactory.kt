package com.lamboapps.bookshop.app.view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lamboapps.bookshop.app.Dependencies
import com.lamboapps.bookshop.app.view.basket.Basket
import com.lamboapps.bookshop.app.view.basket.BasketViewModel
import com.lamboapps.bookshop.app.view.catalogue.Catalogue
import com.lamboapps.bookshop.app.view.catalogue.CatalogueViewModel
import com.lamboapps.bookshop.app.view.detail.Detail
import com.lamboapps.bookshop.app.view.detail.DetailViewModel
import com.lamboapps.bookshop.app.view.main.Main
import com.lamboapps.bookshop.app.view.main.MainViewModel

class ViewModelFactory(private val dependencies: Dependencies) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            BasketViewModel::class.java -> Basket.viewModelInitialState(dependencies)
            CatalogueViewModel::class.java -> Catalogue.viewModelInitialState(dependencies)
            DetailViewModel::class.java -> Detail.viewModelInitialState(dependencies)
            MainViewModel::class.java -> Main.viewModelInitialState(dependencies)
            else -> throw IllegalArgumentException("${modelClass.name} is not defined in factory")
        } as T
    }
}