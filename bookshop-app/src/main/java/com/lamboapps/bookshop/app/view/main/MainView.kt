package com.lamboapps.bookshop.app.view.main

import androidx.compose.Composable
import androidx.compose.Model
import androidx.ui.core.Modifier
import androidx.ui.foundation.Image
import androidx.ui.foundation.Text
import androidx.ui.graphics.ScaleFit
import androidx.ui.layout.fillMaxHeight
import androidx.ui.layout.preferredWidth
import androidx.ui.material.Scaffold
import androidx.ui.material.Tab
import androidx.ui.material.TabRow
import androidx.ui.material.TopAppBar
import androidx.ui.res.vectorResource
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.lamboapps.bookshop.app.LateInitOrDefaultDelegate.Companion.lateInitOrDefault
import com.lamboapps.bookshop.app.R
import com.lamboapps.bookshop.app.view.ui.AppTheme

@Model
class MainView(
    override var tabViewList: List<Main.TabView>,
    override var selectedTab: Int
) : Main.View {

    override var onTabSelected: (Int) -> Unit by lateInitOrDefault { _ -> Unit }

    //Default constructor so we can use @Preview inside the class. Used for preview and ui testing.
    internal constructor() : this(
        tabViewList = (1..3).map {
            Main.TabView(
                iconVectorResId = R.drawable.default_vector_icon_24_black,
                bodyContent = { Text("tab $it") })
        },
        selectedTab = 0
    )

    @Preview
    @Composable
    fun DefaultPreview() {
        MainView().Content()
    }

    @Composable
    fun Content() {
        AppTheme {
            Scaffold(
                topAppBar = { TopTabBar() },
                bodyContent = { SelectedTabContent() }
            )
        }
    }

    @Composable
    private fun TopTabBar() {
        TopAppBar {
            TabRow(
                modifier = Modifier.fillMaxHeight().preferredWidth((70 * tabViewList.size).dp),
                items = tabViewList,
                selectedIndex = selectedTab
            ) { index, item ->
                Tab(modifier = Modifier.fillMaxHeight(),
                    icon = {
                        Image(
                            asset = vectorResource(id = item.iconVectorResId),
                            scaleFit = ScaleFit.FillHeight
                        )
                    },
                    selected = index == selectedTab,
                    onSelected = { onTabSelected(index) }
                )
            }
        }
    }

    @Composable
    private fun SelectedTabContent() {
        tabViewList[selectedTab].bodyContent()
    }
}