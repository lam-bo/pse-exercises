package com.lamboapps.bookshop.app.view.catalogue

import com.lamboapps.bookshop.app.Dependencies
import com.lamboapps.bookshop.app.view.main.Main

object Catalogue {

    fun viewModelInitialState(dependencies: Dependencies): CatalogueViewModel {
        return CatalogueViewModel(
            catalogueInteractor = dependencies.catalogueInteractor,
            navigation = dependencies.navigation,
            view = CatalogueView(
                displayState = DisplayState.FAILURE,
                errorReason = ErrorReason.NONE,
                itemList = emptyList()
            )
        ).apply { setListeners() }
    }

    interface View {
        var displayState: DisplayState
        var errorReason: ErrorReason
        var itemList: List<ItemView>
        var onReloadButtonClick: () -> Unit
        var onCatalogueItemClick: (String) -> Unit
        val mainTabView: Main.TabView
    }
}

enum class DisplayState { LOADING, FAILURE, SUCCESS }

enum class ErrorReason { NONE, NETWORK_ERROR, TECHNICAL_ERROR, NO_ITEMS }

class ItemView(val id: String, val title: String, val priceInCents: Int)