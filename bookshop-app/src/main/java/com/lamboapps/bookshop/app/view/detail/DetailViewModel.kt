package com.lamboapps.bookshop.app.view.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.Either
import com.lamboapps.bookshop.app.view.Navigation
import com.lamboapps.bookshop.core.interactor.DetailInteractor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class DetailViewModel(
    private val navigation: Navigation,
    private val detailInteractor: DetailInteractor,
    val view: Detail.View
) : ViewModel() {

    internal fun setListeners() {
        view.onBackNavigationClick = { navigateBack() }
        view.onAddItemClick = { viewModelScope.launch { addItemToBasket() } }
        view.onRemoveItemClick = { viewModelScope.launch { removeItemFromBasket() } }
    }

    internal suspend fun initView() {
        when (val result = withContext(Dispatchers.IO) { detailInteractor.getCurrentWishes() }) {
            is Either.Right -> result.b.selectedBook?.let {
                view.itemView = ItemView(
                    it.title,
                    it.synopsis,
                    it.coverUrl,
                    it.priceInCents,
                    result.b.isSelectedBookInBasket() ?: false
                )
            }
            is Either.Left -> Unit
        }
    }

    internal fun navigateBack() {
        navigation.goBackToMainView(false)
    }

    internal suspend fun addItemToBasket() {
        withContext(Dispatchers.IO) { detailInteractor.addSelectedBookToBasket() }
        navigation.goBackToMainView(true)
    }

    internal suspend fun removeItemFromBasket() {
        withContext(Dispatchers.IO) { detailInteractor.removeSelectedBookFromBasket() }
        navigation.goBackToMainView(true)
    }
}