package com.lamboapps.bookshop.app.view.ui

import androidx.compose.Composable
import androidx.ui.core.Modifier
import androidx.ui.foundation.Text
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.layout.ColumnScope.gravity
import androidx.ui.text.TextStyle
import androidx.ui.text.font.FontWeight
import androidx.ui.text.style.TextAlign
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import androidx.ui.unit.sp
import java.text.NumberFormat

class BookItemLayout {

    @Preview
    @Composable
    fun DefaultPreview() {
        Content(title = "Title", priceInCents = 1000)
    }

    companion object {
        @Composable
        fun Content(
            title: String,
            priceInCents: Int,
            fontWeight: FontWeight = FontWeight.Normal
        ) {
            Row(modifier = Modifier.preferredHeight(100.dp).fillMaxWidth()) {
                Text(
                    text = title,
                    modifier = Modifier.weight(1f).gravity(ColumnAlign.Center).padding(start = 10.dp),
                    style = TextStyle(
                        fontSize = 20.sp,
                        fontWeight = fontWeight,
                        color = Color.Black
                    )
                )
                PriceText(
                    priceInCents = priceInCents,
                    modifier = Modifier.gravity(ColumnAlign.Center).preferredWidthIn(120.dp)
                )
            }
        }

        @Composable
        fun PriceText(
            prefix: String = "",
            priceInCents: Int,
            modifier: Modifier = Modifier.None,
            textStyle: TextStyle = TextStyle.Default
        ) {
            Text(
                text = "$prefix${NumberFormat.getCurrencyInstance().format(priceInCents / 100f)}",
                modifier = modifier,
                style = TextStyle(
                    color = Color.Black,
                    fontSize = 25.sp,
                    textAlign = TextAlign.Center
                ).merge(textStyle)
            )
        }
    }
}