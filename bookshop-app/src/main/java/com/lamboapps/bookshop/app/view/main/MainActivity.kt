package com.lamboapps.bookshop.app.view.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.ui.core.setContent
import com.lamboapps.bookshop.app.BookshopApp
import com.lamboapps.bookshop.app.view.basket.BasketViewModel
import com.lamboapps.bookshop.app.view.catalogue.CatalogueViewModel

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mainViewModel: MainViewModel by viewModels { (application as BookshopApp).viewModelFactory }
        val catalogueViewModel: CatalogueViewModel by viewModels { (application as BookshopApp).viewModelFactory }
        val basketViewModel: BasketViewModel by viewModels { (application as BookshopApp).viewModelFactory }
        setContent { (mainViewModel.view as MainView).Content() }
        mainViewModel.setTabViewList(
            listOf(
                catalogueViewModel.view.mainTabView,
                basketViewModel.view.mainTabView
            )
        )
    }

    override fun onStart() {
        super.onStart()
        val mainViewModel: MainViewModel by viewModels { (application as BookshopApp).viewModelFactory }
        val basketViewModel: BasketViewModel by viewModels { (application as BookshopApp).viewModelFactory }
        mainViewModel.handleLastNavigationEvent()
        basketViewModel.handleLastNavigationEvent()
    }
}