package com.lamboapps.bookshop.app.repository

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Network
import com.lamboapps.bookshop.core.model.Offer
import com.lamboapps.bookshop.core.model.Technical
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import java.io.IOException


class OffersRemoteSource(retrofit: Retrofit) {

    interface OffersWebService {
        @GET("books/{isbnList}/commercialOffers")
        fun getBookList(@Path("isbnList") isbnListAsString: String): Call<OfferListJson>?
    }

    class OfferListJson {
        var offers: List<OfferJson?>? = null
    }

    class OfferJson {
        var type: String? = null
        var value: Int? = null
        var sliceValue: Int? = null

        fun toOffer(): Offer? {
            return when (type) {
                "percentage" -> Offer(Offer.Type.PERCENTAGE, value ?: 0, null)
                "minus" -> Offer(Offer.Type.MINUS, value ?: 0, null)
                "slice" -> Offer(Offer.Type.SLICE, value ?: 0, sliceValue)
                else -> null
            }
        }
    }

    private val service = retrofit.create(OffersWebService::class.java)

    fun getOffers(isbnList: Iterable<String>): Either<CoreError, Iterable<OfferJson>> {
        return try {
            val response = service.getBookList(isbnList.joinToString(separator = ","))!!.execute()
            Right(sanitize(response.body()))
        } catch (e: IOException) {
            Left(CoreError(Network))
        } catch (e: Exception) {
            Left(CoreError(Technical))
        }
    }

    private fun sanitize(rawResponse: OfferListJson?): Iterable<OfferJson> {
        return mutableListOf<OfferJson>().apply {
            rawResponse?.offers?.forEach { offer ->
                offer?.let { it -> this.add(it) }
            }
        }
    }
}