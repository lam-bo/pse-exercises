package com.lamboapps.bookshop.app

import android.app.Application
import com.lamboapps.bookshop.app.view.ViewModelFactory

class BookshopApp : Application() {

    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate() {
        super.onCreate()
        viewModelFactory = ViewModelFactory(Dependencies(this))
    }
}