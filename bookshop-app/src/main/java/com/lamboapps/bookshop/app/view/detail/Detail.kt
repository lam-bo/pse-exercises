package com.lamboapps.bookshop.app.view.detail

import androidx.lifecycle.viewModelScope
import com.lamboapps.bookshop.app.Dependencies
import kotlinx.coroutines.launch

object Detail {

    fun viewModelInitialState(dependencies: Dependencies): DetailViewModel {
        return DetailViewModel(
            navigation = dependencies.navigation,
            detailInteractor = dependencies.detailInteractor,
            view = DetailView(itemView = null)
        )
            .apply {
                setListeners()
                viewModelScope.launch { initView() }
            }
    }

    interface View {
        var itemView: ItemView?
        var onBackNavigationClick: () -> Unit
        var onAddItemClick: () -> Unit
        var onRemoveItemClick: () -> Unit
    }
}

class ItemView(
    val title: String,
    val synopsis: String,
    val coverUrl: String,
    val priceInCents: Int,
    val isInBasket: Boolean
)