package com.lamboapps.bookshop.app.view.catalogue

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.Either
import com.lamboapps.bookshop.app.view.Navigation
import com.lamboapps.bookshop.core.interactor.CatalogueInteractor
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.Network
import com.lamboapps.bookshop.core.model.Technical
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CatalogueViewModel(
    private val navigation: Navigation,
    private val catalogueInteractor: CatalogueInteractor,
    val view: Catalogue.View
) : ViewModel() {

    fun setListeners() {
        view.onReloadButtonClick = { viewModelScope.launch { reloadItemList() } }
        view.onCatalogueItemClick = { viewModelScope.launch { selectBook(it) } }
    }

    internal suspend fun reloadItemList() {
        view.displayState = DisplayState.LOADING
        when (val result = withContext(Dispatchers.IO) { catalogueInteractor.getBookList() }) {
            is Either.Left -> {
                when (result.a.marker) {
                    is Network -> reloadFailure(ErrorReason.NETWORK_ERROR)
                    is Technical -> reloadFailure(ErrorReason.TECHNICAL_ERROR)
                    else -> reloadFailure(ErrorReason.TECHNICAL_ERROR)
                }
            }
            is Either.Right -> {
                when (result.b.isEmpty()) {
                    true -> reloadFailure(ErrorReason.NO_ITEMS)
                    false -> reloadSuccess(result.b)
                }
            }
        }
    }

    private fun reloadFailure(errorReason: ErrorReason) {
        view.displayState = DisplayState.FAILURE
        view.errorReason = errorReason
        view.itemList = emptyList()
    }

    private fun reloadSuccess(bookList: List<Book>) {
        view.displayState = DisplayState.SUCCESS
        view.errorReason = ErrorReason.NONE
        view.itemList = bookList.map { ItemView(it.isbn, it.title, it.priceInCents) }
    }

    internal suspend fun selectBook(bookId: String) {
        val result = withContext(Dispatchers.IO) { catalogueInteractor.setSelectedBook(bookId) }
        if (result.isRight()) {
            navigation.goToSelectedArticleView()
        }
    }
}