package com.lamboapps.bookshop.app.view.basket

import androidx.compose.Composable
import androidx.compose.Model
import androidx.ui.core.Modifier
import androidx.ui.foundation.*
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.layout.ColumnScope.gravity
import androidx.ui.material.*
import androidx.ui.material.ripple.ripple
import androidx.ui.res.stringResource
import androidx.ui.res.vectorResource
import androidx.ui.text.TextStyle
import androidx.ui.text.font.FontWeight
import androidx.ui.unit.dp
import androidx.ui.unit.sp
import com.lamboapps.bookshop.app.LateInitOrDefaultDelegate.Companion.lateInitOrDefault
import com.lamboapps.bookshop.app.R
import com.lamboapps.bookshop.app.view.main.Main
import com.lamboapps.bookshop.app.view.ui.AppTheme
import com.lamboapps.bookshop.app.view.ui.BookItemLayout
import com.lamboapps.bookshop.app.view.ui.UserMessageLayout
import com.lamboapps.bookshop.app.view.ui.buttonTextStyle

@Model
class BasketView(
    override var displayState: DisplayState,
    override var errorReason: ErrorReason,
    override var itemList: List<BasketItemView>,
    override var basketPrices: BasketPricesView
) : Basket.View {

    override var onReloadButtonClick: () -> Unit by lateInitOrDefault { Unit }
    override var onBasketItemClick: (String) -> Unit by lateInitOrDefault { _ -> Unit }
    override var onPaymentButtonClick: () -> Unit by lateInitOrDefault { Unit }
    override val mainTabView = Main.TabView(
        iconVectorResId = R.drawable.ic_baseline_shopping_cart_24,
        bodyContent = @Composable { Content() }
    )

    @Composable
    fun Content() {
        AppTheme {
            when (displayState) {
                DisplayState.LOADING -> LoadingContent()
                DisplayState.SUCCESS -> SuccessContent()
                DisplayState.FAILURE -> FailureContent()
            }
        }
    }

    @Composable
    fun LoadingContent() {
        UserMessageLayout.Content(text = stringResource(id = R.string.loading_in_progress)) {
            CircularProgressIndicator(
                modifier = Modifier.gravity(ColumnAlign.Center).preferredSize(65.dp)
            )
        }
    }

    @Composable
    fun FailureContent() {
        val (message, displayReloadButton) = when (errorReason) {
            ErrorReason.NONE -> Pair("", false)
            ErrorReason.NETWORK_ERROR -> Pair(stringResource(id = R.string.error_network), true)
            ErrorReason.TECHNICAL_ERROR -> Pair(stringResource(id = R.string.error_technical), true)
            ErrorReason.NO_ITEMS -> Pair(stringResource(id = R.string.error_basket_no_items), false)
        }
        UserMessageLayout.Content(text = message) {
            if (displayReloadButton) {
                Button(
                    onClick = onReloadButtonClick,
                    modifier = Modifier.gravity(ColumnAlign.Center),
                    backgroundColor = MaterialTheme.colors.secondary
                ) {
                    Text(
                        text = stringResource(id = R.string.reload_basket).toUpperCase(),
                        style = buttonTextStyle
                    )
                }
            }
        }
    }


    @Composable
    fun SuccessContent() {
        Scaffold(
            floatingActionButtonPosition = Scaffold.FabPosition.EndDocked,
            floatingActionButton = { PaymentFabButton() },
            bottomAppBar = { BasketPrices(it) }) {
            BasketItems()
        }
    }

    @Composable
    fun BasketItems() {
        Surface(modifier = Modifier.padding(start = 20.dp, end = 20.dp).fillMaxSize()) {
            VerticalScroller(modifier = Modifier.fillMaxSize()) {
                Column {
                    for (item in itemList) {
                        Row(modifier = Modifier.fillMaxWidth()) {

                            Clickable(
                                onClick = { onBasketItemClick(item.id) },
                                modifier = Modifier.ripple(bounded = true)
                            ) {
                                BookItemLayout.Content(
                                    title = item.title,
                                    priceInCents = item.priceInCents
                                )
                            }
                        }
                    }
                    Box(modifier = Modifier.fillMaxWidth().preferredHeight(200.dp))
                }
            }
        }
    }

    @Composable
    fun PaymentFabButton() {
        FloatingActionButton(
            onClick = {},
            shape = RoundedCornerShape(50),
            backgroundColor = MaterialTheme.colors.secondary
        ) {
            IconButton(onClick = onPaymentButtonClick) {
                Icon(
                    asset = vectorResource(id = R.drawable.ic_baseline_payment_24),
                    tint = Color.White
                )
            }
        }
    }

    @Composable
    fun BasketPrices(fabConfiguration: BottomAppBar.FabConfiguration?) {
        BottomAppBar(
            modifier = Modifier.preferredHeight(150.dp),
            fabConfiguration = fabConfiguration,
            cutoutShape = RoundedCornerShape(50)
        ) {
            Column(modifier = Modifier.padding(start = 40.dp)) {
                BookItemLayout.PriceText(
                    prefix = stringResource(id = R.string.base_price),
                    priceInCents = basketPrices.basePriceInCents,
                    textStyle = TextStyle(color = Color.White, fontSize = 20.sp)
                )
                BookItemLayout.PriceText(
                    prefix = stringResource(id = R.string.offer_price),
                    priceInCents = basketPrices.offerPriceInCents,
                    textStyle = TextStyle(color = Color.White, fontSize = 20.sp)
                )
                BookItemLayout.PriceText(
                    prefix = stringResource(id = R.string.full_price),
                    priceInCents = basketPrices.fullPriceInCents,
                    textStyle = TextStyle(
                        color = Color.White,
                        fontSize = 25.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
            }
        }
    }

}

