package com.lamboapps.bookshop.app.view.main

import androidx.lifecycle.ViewModel
import com.lamboapps.bookshop.app.view.Navigation

class MainViewModel(
    private val navigation: Navigation,
    val view: Main.View
) : ViewModel() {
    companion object {
        const val BASKET_TAB_INDEX: Int = 1
    }

    private var lastSeenEvent: Navigation.Event? = null

    internal fun setListeners() {
        view.onTabSelected = { selectTab(it) }
    }

    fun setTabViewList(tabViewList: List<Main.TabView>) {
        view.tabViewList = tabViewList
    }

    internal fun selectTab(index: Int) {
        if (index != view.selectedTab) {
            view.selectedTab = index
        }
    }

    internal fun handleLastNavigationEvent() {
        with(navigation.lastNavigationEvent) {
            if (this != lastSeenEvent && this?.action is Navigation.Marker.FocusOnBasket) {
                lastSeenEvent = this
                selectTab(BASKET_TAB_INDEX)
            }
        }
    }
}
