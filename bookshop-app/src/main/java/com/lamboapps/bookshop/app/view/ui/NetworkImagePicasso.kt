package com.lamboapps.bookshop.app.view.ui

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.compose.Composable
import androidx.compose.onCommit
import androidx.compose.state
import androidx.ui.core.Modifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.Canvas
import androidx.ui.foundation.ContentGravity
import androidx.ui.foundation.Image
import androidx.ui.graphics.ImageAsset
import androidx.ui.graphics.ScaleFit
import androidx.ui.graphics.asImageAsset
import androidx.ui.layout.fillMaxWidth
import androidx.ui.layout.preferredHeight
import androidx.ui.unit.dp
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target


//https://github.com/vinaygaba/Learn-Jetpack-Compose-By-Example/blob/master/app/src/main/java/com/example/jetpackcompose/image/ImageActivity.kt#L161
class NetworkImagePicasso {

    companion object {
        @Composable
        fun Content(
            url: String,
            modifier: Modifier = Modifier.fillMaxWidth() +
                    Modifier.preferredHeight(height = 200.dp)
        ) {
            var image by state<ImageAsset?> { null }
            var drawable by state<Drawable?> { null }
            onCommit(url) {
                val picasso = Picasso.get()
                val target = object : Target {
                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        // TODO(lmr): we could use the drawable below
                        drawable = placeHolderDrawable
                    }

                    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                        drawable = errorDrawable
                    }

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        image = bitmap?.asImageAsset()
                    }
                }
                picasso
                    .load(url)
                    .into(target)

                onDispose {
                    image = null
                    drawable = null
                    picasso.cancelRequest(target)
                }
            }

            val theImage = image
            val theDrawable = drawable
            if (theImage != null) {
                Box(
                    modifier = modifier,
                    gravity = ContentGravity.Center
                ) {
                    // Image is a pre-defined composable that lays out and draws a given [ImageAsset].
                    Image(
                        asset = theImage,
                        //TODO extract scale fit
                        scaleFit = ScaleFit.FillMaxDimension
                    )
                }
            } else if (theDrawable != null) {
                Canvas(modifier = modifier) {
                    theDrawable.draw(this.nativeCanvas)
                }
            }
        }
    }
}