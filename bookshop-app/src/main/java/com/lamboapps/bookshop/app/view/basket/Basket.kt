package com.lamboapps.bookshop.app.view.basket

import com.lamboapps.bookshop.app.Dependencies
import com.lamboapps.bookshop.app.view.main.Main


object Basket {

    fun viewModelInitialState(dependencies: Dependencies): BasketViewModel {
        return BasketViewModel(
            navigation = dependencies.navigation,
            basketInteractor = dependencies.basketInteractor,
            view = BasketView(
                displayState = DisplayState.FAILURE,
                errorReason = ErrorReason.NO_ITEMS,
                itemList = emptyList(),
                basketPrices = BasketPricesView(0, 0, 0)
            )
        ).apply {
            setListeners()
        }
    }


    interface View {
        var displayState: DisplayState
        var errorReason: ErrorReason
        var itemList: List<BasketItemView>
        var basketPrices: BasketPricesView
        var onReloadButtonClick: () -> Unit
        var onBasketItemClick: (String) -> Unit
        var onPaymentButtonClick: () -> Unit
        val mainTabView: Main.TabView
    }
}

enum class DisplayState { LOADING, FAILURE, SUCCESS }

enum class ErrorReason { NONE, NETWORK_ERROR, TECHNICAL_ERROR, NO_ITEMS }

class BasketItemView(val id: String, val title: String, val priceInCents: Int)

class BasketPricesView(
    val basePriceInCents: Int,
    val offerPriceInCents: Int,
    val fullPriceInCents: Int
)