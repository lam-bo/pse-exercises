package com.lamboapps.bookshop.app.view.ui

import androidx.compose.Composable
import androidx.ui.core.Modifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.ContentGravity
import androidx.ui.foundation.Image
import androidx.ui.foundation.Text
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.material.Surface
import androidx.ui.res.imageResource
import androidx.ui.text.TextStyle
import androidx.ui.text.style.TextAlign
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import androidx.ui.unit.sp
import com.lamboapps.bookshop.app.R

class UserMessageLayout {

    @Preview
    @Composable
    fun DefaultPreview() {
        Content(
            text = "message",
            children = { Image(imageResource(id = R.drawable.default_vector_icon_24_black)) })
    }

    companion object {
        @Composable
        fun Content(text: String, children: @Composable() () -> Unit) {
            Box(
                modifier = Modifier.fillMaxSize(),
                gravity = ContentGravity.Center
            ) {
                Column(modifier = Modifier.fillMaxWidth()) {
                    Surface(
                        modifier = Modifier.gravity(ColumnAlign.Center).padding(20.dp),
                        color = Color.Transparent
                    ) {
                        Text(
                            text = text,
                            style = TextStyle(
                                color = Color.Black,
                                fontSize = 25.sp,
                                textAlign = TextAlign.Center
                            )
                        )
                    }
                    children()
                }
            }
        }
    }
}

