package com.lamboapps.bookshop.app.view.catalogue

import androidx.compose.Composable
import androidx.compose.Model
import androidx.ui.core.Modifier
import androidx.ui.foundation.Clickable
import androidx.ui.foundation.Text
import androidx.ui.foundation.VerticalScroller
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.layout.ColumnScope.gravity
import androidx.ui.material.Button
import androidx.ui.material.Card
import androidx.ui.material.CircularProgressIndicator
import androidx.ui.material.MaterialTheme
import androidx.ui.material.ripple.ripple
import androidx.ui.res.stringResource
import androidx.ui.text.font.FontWeight
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.lamboapps.bookshop.app.LateInitOrDefaultDelegate.Companion.lateInitOrDefault
import com.lamboapps.bookshop.app.R
import com.lamboapps.bookshop.app.view.main.Main
import com.lamboapps.bookshop.app.view.ui.AppTheme
import com.lamboapps.bookshop.app.view.ui.BookItemLayout
import com.lamboapps.bookshop.app.view.ui.UserMessageLayout
import com.lamboapps.bookshop.app.view.ui.buttonTextStyle


@Model
class CatalogueView(
    override var displayState: DisplayState,
    override var errorReason: ErrorReason,
    override var itemList: List<ItemView>
) : Catalogue.View {

    override var onReloadButtonClick: () -> Unit by lateInitOrDefault { Unit }
    override var onCatalogueItemClick: (String) -> Unit by lateInitOrDefault { _ -> Unit }
    override val mainTabView = Main.TabView(R.drawable.ic_baseline_search_24) { Content() }

    internal constructor() : this(
        displayState = DisplayState.SUCCESS,
        errorReason = ErrorReason.NONE,
        itemList = (1..5).map { ItemView("$it", "title $it", 1000) }
    )

    @Preview
    @Composable
    fun DefaultPreview() {
        CatalogueView().Content()
    }

    @Composable
    fun Content() {
        AppTheme {
            when (displayState) {
                DisplayState.LOADING -> LoadingContent()
                DisplayState.FAILURE -> FailureContent()
                DisplayState.SUCCESS -> SuccessContent()
            }
        }
    }

    @Composable
    fun LoadingContent() {
        UserMessageLayout.Content(text = stringResource(id = R.string.loading_in_progress)) {
            CircularProgressIndicator(
                modifier = Modifier.gravity(ColumnAlign.Center).preferredSize(65.dp)
            )
        }
    }

    @Composable
    fun FailureContent() {
        val message = when (errorReason) {
            ErrorReason.NONE -> ""
            ErrorReason.NETWORK_ERROR -> stringResource(id = R.string.error_network)
            ErrorReason.TECHNICAL_ERROR -> stringResource(id = R.string.error_technical)
            ErrorReason.NO_ITEMS -> stringResource(id = R.string.error_no_items)
        }
        UserMessageLayout.Content(text = message) {
            Button(
                onClick = onReloadButtonClick,
                backgroundColor = MaterialTheme.colors.secondary,
                modifier = Modifier.gravity(ColumnAlign.Center)
            ) {
                Text(
                    stringResource(id = R.string.reload_catalogue).toUpperCase(),
                    style = buttonTextStyle
                )
            }
        }
    }


    @Composable
    fun SuccessContent() {
        VerticalScroller(modifier = Modifier.fillMaxHeight()) {
            Column {
                for (item in itemList) {
                    Row(modifier = Modifier.padding(4.dp) + Modifier.fillMaxWidth()) {
                        Card(
                            shape = RoundedCornerShape(4.dp), color = Color.White,
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            Clickable(
                                onClick = { onCatalogueItemClick(item.id) },
                                modifier = Modifier.ripple(bounded = true)
                            ) {
                                BookItemLayout.Content(
                                    title = item.title,
                                    priceInCents = item.priceInCents,
                                    fontWeight = FontWeight.Bold
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}
