package com.lamboapps.bookshop.app

import java.util.concurrent.atomic.AtomicBoolean
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Delegates for values that can be set only once. If not set will return default value
 */
class LateInitOrDefaultDelegate<T>(defaultValue: T) : ReadWriteProperty<Any, T> {
    private var value: T = defaultValue
    private val initialized: AtomicBoolean = AtomicBoolean(false)

    companion object {
        fun <T> lateInitOrDefault(defaultValue: T) = LateInitOrDefaultDelegate(defaultValue)
    }

    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        return value
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        if (initialized.get()) {
            throw IllegalStateException("Value is initialized")
        } else {
            initialized.set(true)
            this.value = value
        }
    }
}
