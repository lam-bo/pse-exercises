package com.lamboapps.bookshop.app.view.detail

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.ui.core.setContent
import com.lamboapps.bookshop.app.BookshopApp

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val detailViewModel: DetailViewModel by viewModels { (application as BookshopApp).viewModelFactory }
        setContent { (detailViewModel.view as DetailView).Content() }
    }
}