package com.lamboapps.bookshop.app

import android.content.Context
import com.lamboapps.bookshop.app.repository.*
import com.lamboapps.bookshop.app.view.Navigation
import com.lamboapps.bookshop.core.interactor.BasketInteractor
import com.lamboapps.bookshop.core.interactor.CatalogueInteractor
import com.lamboapps.bookshop.core.interactor.DetailInteractor
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.Offer
import com.lamboapps.bookshop.core.model.Wishes
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Simple dependency injector for Bookshop app, all dependencies here have app lifecycle
 */
class Dependencies(private val appContext: Context) {

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("http://henri-potier.xebia.fr")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val navigation: Navigation by lazy { Navigation(appContext) }

    private val booksRemoteSource: BooksRemoteSource by lazy { BooksRemoteSource(retrofit) }
    private val bookRepository: Book.Repository by lazy { BookRepository(booksRemoteSource) }
    private val wishesRepository: Wishes.Repository by lazy { WishesRepository() }
    private val offersRemoteSource: OffersRemoteSource by lazy { OffersRemoteSource(retrofit) }
    private val offerRepository: Offer.Repository by lazy { OfferRepository(offersRemoteSource) }

    val catalogueInteractor: CatalogueInteractor by lazy {
        CatalogueInteractor(
            bookRepository,
            wishesRepository
        )
    }

    val detailInteractor: DetailInteractor by lazy { DetailInteractor(wishesRepository) }

    val basketInteractor: BasketInteractor by lazy {
        BasketInteractor(
            wishesRepository,
            offerRepository
        )
    }
}