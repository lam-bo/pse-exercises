package com.lamboapps.bookshop.app.repository

import arrow.core.Either
import arrow.core.Right
import com.lamboapps.bookshop.app.repository.OffersRemoteSource.OfferJson
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Offer

class OfferRepository(private val offersRemoteSource: OffersRemoteSource) : Offer.Repository {

    internal var cachedIsbnSet: Set<String> = emptySet()
    internal var cachedOfferList: Iterable<Offer> = emptyList()

    override fun getAllOffers(isbnSet: Set<String>): Either<CoreError, Iterable<Offer>> {
        return when {
            isbnSet.isEmpty() -> Right(emptyList())
            isbnSet == cachedIsbnSet -> Right(cachedOfferList)
            else -> offersRemoteSource.getOffers(isbnSet).map {
                jsonToModel(it).apply {
                    cachedIsbnSet = isbnSet
                    cachedOfferList = this
                }
            }
        }
    }

    private fun jsonToModel(jsonOffers: Iterable<OfferJson>): Iterable<Offer> {
        return jsonOffers.fold(mutableListOf()) { acc, jsonOffer ->
            jsonOffer.toOffer()?.apply { acc.add(this) }
            acc
        }
    }
}


