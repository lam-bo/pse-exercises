package com.lamboapps.bookshop.app.repository

import arrow.core.Either
import arrow.core.Right
import arrow.core.extensions.list.foldable.nonEmpty
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.CoreError

class BookRepository(private val booksRemoteSource: BooksRemoteSource) : Book.Repository {

    internal var cachedBooks: Iterable<Book> = emptyList()

    override fun getAllBooks(): Either<CoreError, Iterable<Book>> {
        return if (cachedBooks.toList().nonEmpty()) {
            Right(cachedBooks)
        } else {
            booksRemoteSource.getBooks().map {
                jsonToModel(it).apply { cachedBooks = this }
            }
        }
    }

    private fun jsonToModel(jsonBooks: Iterable<BooksRemoteSource.BookJsonModel>): List<Book> {
        return jsonBooks.fold(mutableListOf()) { acc, jsonBook ->
            jsonBook.toBook()?.apply { acc.add(this) }
            acc
        }
    }
}