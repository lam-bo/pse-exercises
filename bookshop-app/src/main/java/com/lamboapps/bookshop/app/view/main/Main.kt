package com.lamboapps.bookshop.app.view.main

import androidx.compose.Composable
import com.lamboapps.bookshop.app.Dependencies

object Main {

    fun viewModelInitialState(dependencies: Dependencies): MainViewModel {
        return MainViewModel(
            navigation = dependencies.navigation,
            view = MainView()
        ).apply { setListeners() }
    }

    class TabView(val iconVectorResId: Int, val bodyContent: @Composable() () -> Unit)

    //Necessary for testing purposes because @Model can't be fully mocked
    interface View {
        var tabViewList: List<TabView>
        var selectedTab: Int
        var onTabSelected: (Int) -> Unit
    }
}