package com.lamboapps.bookshop.app.view.detail

import androidx.compose.Composable
import androidx.compose.Model
import androidx.ui.core.Modifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.Icon
import androidx.ui.foundation.Text
import androidx.ui.foundation.VerticalScroller
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.material.*
import androidx.ui.res.stringResource
import androidx.ui.res.vectorResource
import androidx.ui.text.TextStyle
import androidx.ui.text.font.FontWeight
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.lamboapps.bookshop.app.LateInitOrDefaultDelegate.Companion.lateInitOrDefault
import com.lamboapps.bookshop.app.R
import com.lamboapps.bookshop.app.view.ui.AppTheme
import com.lamboapps.bookshop.app.view.ui.BookItemLayout
import com.lamboapps.bookshop.app.view.ui.NetworkImagePicasso

@Model
class DetailView(
    override var itemView: ItemView?
) : Detail.View {

    override var onBackNavigationClick: () -> Unit by lateInitOrDefault { Unit }
    override var onAddItemClick: () -> Unit by lateInitOrDefault { Unit }
    override var onRemoveItemClick: () -> Unit by lateInitOrDefault { Unit }

    constructor() : this(
        ItemView(
            "title",
            "synopsis",
            "http://henri-potier.xebia.fr/hp0.jpg",
            1000,
            false
        )
    )

    @Preview
    @Composable
    fun DefaultPreview() {
        DetailView().Content()
    }

    @Composable
    fun Content() {
        AppTheme {
            Scaffold(
                topAppBar = { TopBar() },
                bodyContent = { BookDetail() },
                floatingActionButtonPosition = Scaffold.FabPosition.EndDocked,
                floatingActionButton = { BasketFabButton() },
                bottomAppBar = { CutoutBottomBar(it) }
            )
        }
    }

    @Composable
    private fun TopBar() {
        TopAppBar(
            title = { Text(text = stringResource(id = R.string.article_details)) },
            navigationIcon = {
                IconButton(onClick = onBackNavigationClick) {
                    Icon(asset = vectorResource(id = R.drawable.ic_baseline_arrow_back_24))
                }
            }
        )
    }

    @Composable
    private fun BookDetail() {
        Surface(modifier = Modifier.fillMaxSize().padding(start = 10.dp, end = 10.dp)) {
            VerticalScroller {
                Column {
                    BookItemLayout.Content(
                        title = itemView?.title ?: "null",
                        priceInCents = itemView?.priceInCents ?: 0,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        text = itemView?.synopsis ?: "",
                        style = TextStyle(color = Color.Black),
                        modifier = Modifier.padding(10.dp)
                    )
                    if (itemView?.coverUrl != null) {
                        NetworkImagePicasso.Content(
                            modifier = Modifier.preferredHeight(300.dp).fillMaxWidth().padding(end = 60.dp),
                            url = itemView!!.coverUrl
                        )
                    }
                    Box(modifier = Modifier.preferredHeight(60.dp))
                }
            }
        }
    }

    @Composable
    fun BasketFabButton() {
        FloatingActionButton(
            onClick = {},
            shape = RoundedCornerShape(50),
            backgroundColor = if (itemView?.isInBasket == true) MaterialTheme.colors.error else MaterialTheme.colors.secondary
        ) {
            when (itemView?.isInBasket) {
                true -> IconButton(onClick = onRemoveItemClick) {
                    Icon(
                        asset = vectorResource(id = R.drawable.ic_baseline_remove_shopping_cart_24),
                        tint = Color.White
                    )
                }
                false -> IconButton(onClick = onAddItemClick) {
                    Icon(
                        asset = vectorResource(id = R.drawable.ic_baseline_add_shopping_cart_24),
                        tint = Color.White
                    )
                }
                null -> Unit
            }
        }
    }

    @Composable
    fun CutoutBottomBar(fabConfiguration: BottomAppBar.FabConfiguration?) {
        BottomAppBar(
            fabConfiguration = fabConfiguration,
            cutoutShape = RoundedCornerShape(50)
        ) {}
    }
}