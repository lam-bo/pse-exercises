package com.lamboapps.bookshop.app.repository

import arrow.core.Either
import arrow.core.Right
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Wishes


class WishesRepository : Wishes.Repository {

    internal var selectedBook: Book? = null
    internal val books: MutableSet<Book> = mutableSetOf()

    override fun setSelectedBook(book: Book): Either<CoreError, Unit> =
        Right(Unit).apply { selectedBook = book }

    override fun resetSelectedBook(): Either<CoreError, Unit> =
        Right(Unit).apply { selectedBook = null }

    override fun getCurrentWishes(): Either<CoreError, Wishes> =
        Right(Wishes(selectedBook, books))

    override fun addBookToBasket(book: Book): Either<CoreError, Unit> {
        books.add(book)
        return Right(Unit)
    }

    override fun removeBookFromBasket(book: Book): Either<CoreError, Unit> {
        books.remove(book)
        return Right(Unit)
    }
}