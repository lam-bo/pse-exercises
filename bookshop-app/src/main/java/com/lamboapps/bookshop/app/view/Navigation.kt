package com.lamboapps.bookshop.app.view

import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.lamboapps.bookshop.app.R
import com.lamboapps.bookshop.app.view.detail.DetailActivity
import com.lamboapps.bookshop.app.view.main.MainActivity
import java.util.*

/**
 * Wrapper around app context to start activities without exposing context
 */
class Navigation(private val appContext: Context) {

    var lastNavigationEvent: Event? = null

    fun goToSelectedArticleView() {
        appContext.startActivity(Intent(appContext, DetailActivity::class.java))
    }

    fun goBackToMainView(focusOnBasket: Boolean) {
        if (focusOnBasket) {
            lastNavigationEvent = Event(Marker.FocusOnBasket)
        }
        val intent = Intent(appContext, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }
        appContext.startActivity(intent)
    }

    fun goToPayment() {
        Toast.makeText(appContext, R.string.payment_message, Toast.LENGTH_SHORT).show()
    }

    data class Event(val action: Marker, val tag: String = UUID.randomUUID().toString())

    sealed class Marker {
        object FocusOnBasket : Marker()
    }
}