package com.lamboapps.bookshop.app.repository

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Network
import com.lamboapps.bookshop.core.model.Technical
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.GET
import java.io.IOException


class BooksRemoteSource(retrofit: Retrofit) {

    interface BooksWebService {
        @GET("books")
        fun getBookList(): Call<List<BookJsonModel>>?
    }

    class BookJsonModel {
        var isbn: String? = null
        var title: String? = null
        var price: Int? = null
        var cover: String? = null
        var synopsis: List<String>? = null

        fun toBook(): Book? {
            return if (isbn != null && title != null && price != null && cover != null && !synopsis.isNullOrEmpty()) {
                Book(isbn!!, title!!, synopsis!!.first(), cover!!, price!! * 100)
            } else null
        }
    }

    private val service = retrofit.create(BooksWebService::class.java)

    fun getBooks(): Either<CoreError, Iterable<BookJsonModel>> {
        return try {
            val response = service.getBookList()!!.execute()
            Right(sanitize(response.body()))
        } catch (e: IOException) {
            Left(CoreError(Network))
        } catch (e: Exception) {
            Left(CoreError(Technical))
        }
    }

    private fun sanitize(rawResponse: List<BookJsonModel?>?): Iterable<BookJsonModel> {
        return mutableListOf<BookJsonModel>().apply {
            rawResponse?.forEach { book ->
                book?.let { it -> this.add(it) }
            }
        }
    }
}





