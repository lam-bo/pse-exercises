package com.lamboapps.bookshop.app.view.ui

import androidx.compose.Composable
import androidx.ui.graphics.Color
import androidx.ui.material.MaterialTheme
import androidx.ui.material.lightColorPalette
import androidx.ui.text.TextStyle
import androidx.ui.text.font.FontWeight
import androidx.ui.unit.sp

object AppStyles {

    val themeColors = lightColorPalette(
        primary = Color(0xFF006064),
        secondary = Color(0xFF1565c0),
        error = Color(0xFFb71c1c),
        background = Color(0xFFefebe9),
        onSurface = Color.White
    )

}

@Composable
fun AppTheme(children: @Composable() () -> Unit) {
    MaterialTheme(colors = AppStyles.themeColors) {
        children()
    }
}

val buttonTextStyle = TextStyle(
    color = Color.White,
    fontWeight = FontWeight.Bold,
    fontSize = 16.sp
)


