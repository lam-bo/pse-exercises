package com.lamboapps.bookshop.app.view.basket

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.Either
import com.lamboapps.bookshop.app.view.Navigation
import com.lamboapps.bookshop.core.interactor.BasketInteractor
import com.lamboapps.bookshop.core.model.Network
import com.lamboapps.bookshop.core.model.Technical
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BasketViewModel(
    private val navigation: Navigation,
    private val basketInteractor: BasketInteractor,
    val view: Basket.View
) : ViewModel() {

    internal var lastSeenEvent: Navigation.Event? = null

    internal fun setListeners() {
        view.onReloadButtonClick = { viewModelScope.launch { reloadBasketWithPrices() } }
        view.onBasketItemClick = { viewModelScope.launch { selectBook(it) } }
        view.onPaymentButtonClick = { payBasket() }
    }

    internal fun handleLastNavigationEvent() {
        viewModelScope.launch { reloadBasketWithPrices() }
        with(navigation.lastNavigationEvent) {
            if (this != lastSeenEvent && this?.action is Navigation.Marker.FocusOnBasket) {
                lastSeenEvent = this
                viewModelScope.launch { reloadBasketWithPrices() }
            }
        }
    }

    internal suspend fun reloadBasketWithPrices() {
        view.displayState = DisplayState.LOADING
        when (val result = withContext(Dispatchers.IO) { basketInteractor.getBasketWithPrices() }) {
            is Either.Left -> when (result.a.marker) {
                is Network -> reloadFailure(ErrorReason.NETWORK_ERROR)
                is Technical -> reloadFailure(ErrorReason.TECHNICAL_ERROR)
                else -> reloadFailure(ErrorReason.TECHNICAL_ERROR)
            }
            is Either.Right -> if (result.b.books.isNotEmpty()) {
                reloadSuccess(result.b)
            } else {
                reloadFailure(ErrorReason.NO_ITEMS)
            }
        }
    }

    private fun reloadFailure(errorReason: ErrorReason) {
        view.displayState = DisplayState.FAILURE
        view.errorReason = errorReason
        view.itemList = emptyList()
        view.basketPrices = BasketPricesView(0, 0, 0)
    }

    private fun reloadSuccess(basket: BasketInteractor.BasketWithPrices) {
        view.displayState = DisplayState.SUCCESS
        view.itemList = basket.books.map { BasketItemView(it.isbn, it.title, it.priceInCents) }
        view.basketPrices = BasketPricesView(
            basket.basePriceInCents,
            basket.offerPriceInCents,
            basket.fullPriceInCents
        )
    }

    internal suspend fun selectBook(bookId: String) {
        val result = withContext(Dispatchers.IO) { basketInteractor.setSelectedBook(bookId) }
        if (result.isRight()) {
            navigation.goToSelectedArticleView()
        }
    }

    internal fun payBasket() {
        navigation.goToPayment()
    }
}