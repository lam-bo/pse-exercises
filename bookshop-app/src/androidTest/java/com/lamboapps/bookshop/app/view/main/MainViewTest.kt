package com.lamboapps.bookshop.app.view.main

import androidx.test.filters.MediumTest
import androidx.ui.test.assertIsDisplayed
import androidx.ui.test.createComposeRule
import androidx.ui.test.findByText
import com.nhaarman.mockitokotlin2.spy
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


/**
 * Proof of concept to see if it's easy to test the view with the current code architecture
 * Will be generalized once Jetpack Compose is out of beta, still work in progress for now
 */
@MediumTest
@RunWith(JUnit4::class)
@Ignore("Work in progress")
class MainViewTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    private val mainView: MainView = spy(MainView())

    @Before
    fun setUp() {
        composeTestRule.setContent {
            mainView.Content()
        }
    }

    @Test
    fun onInitialState_shouldTabContentBeVisible() {
        findByText("Text").assertIsDisplayed()
    }
}