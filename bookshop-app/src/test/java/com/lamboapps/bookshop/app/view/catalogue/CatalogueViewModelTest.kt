package com.lamboapps.bookshop.app.view.catalogue

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.lamboapps.bookshop.app.view.Navigation
import com.lamboapps.bookshop.core.interactor.CatalogueInteractor
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Network
import com.lamboapps.bookshop.core.model.Technical
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.runBlocking
import org.junit.Test

class CatalogueViewModelTest {

    @Test
    fun setListeners_shouldSetThemAll() {
        val viewModel = CatalogueViewModel(mock(), mock(), mock())

        viewModel.setListeners()

        verify(viewModel.view).onCatalogueItemClick = any()
        verify(viewModel.view).onReloadButtonClick = any()
    }

    @Test
    fun reloadItemsList_shouldLoadAndDisplaySuccess_whenNotEmptyItemsList() {
        val book = Book("id", "title", "synopsis", "cover", 1000)
        val catalogueInteractor: CatalogueInteractor = mock {
            on(it.getBookList()).thenReturn(Either.Right(listOf(book)))
        }
        val view: Catalogue.View = mock()
        val viewModel = CatalogueViewModel(mock(), catalogueInteractor, view)

        runBlocking { viewModel.reloadItemList() }

        inOrder(view, catalogueInteractor) {
            verify(view).displayState = DisplayState.LOADING
            verify(catalogueInteractor).getBookList()
            verify(view).displayState = DisplayState.SUCCESS
            verify(view).errorReason = ErrorReason.NONE
            verify(view).itemList = any()
        }
    }

    @Test
    fun reloadItemsList_shouldLoadAndDisplayFailure_whenEmptyItemsList() {
        val catalogueInteractor: CatalogueInteractor = mock {
            on(it.getBookList()).thenReturn(Either.Right(emptyList()))
        }
        val view: Catalogue.View = mock()
        val viewModel = CatalogueViewModel(mock(), catalogueInteractor, view)

        runBlocking { viewModel.reloadItemList() }

        inOrder(view, catalogueInteractor) {
            verify(view).displayState = DisplayState.LOADING
            verify(catalogueInteractor).getBookList()
            verify(view).displayState = DisplayState.FAILURE
            verify(view).errorReason = ErrorReason.NO_ITEMS
            verify(view).itemList = emptyList()
        }
    }

    @Test
    fun reloadItemsList_shouldLoadAndDisplayFailure_whenNetworkError() {
        val catalogueInteractor: CatalogueInteractor = mock {
            on(it.getBookList()).thenReturn(Left(CoreError(Network)))
        }
        val view: Catalogue.View = mock()
        val viewModel = CatalogueViewModel(mock(), catalogueInteractor, view)

        runBlocking { viewModel.reloadItemList() }

        inOrder(view, catalogueInteractor) {
            verify(view).displayState = DisplayState.LOADING
            verify(catalogueInteractor).getBookList()
            verify(view).displayState = DisplayState.FAILURE
            verify(view).errorReason = ErrorReason.NETWORK_ERROR
            verify(view).itemList = emptyList()
        }
    }

    @Test
    fun reloadItemsList_shouldLoadAndDisplayFailure_whenTechnicalError() {
        val catalogueInteractor: CatalogueInteractor = mock {
            on(it.getBookList()).thenReturn(Left(CoreError(Technical)))
        }
        val view: Catalogue.View = mock()
        val viewModel = CatalogueViewModel(mock(), catalogueInteractor, view)

        runBlocking { viewModel.reloadItemList() }

        inOrder(view, catalogueInteractor) {
            verify(view).displayState = DisplayState.LOADING
            verify(catalogueInteractor).getBookList()
            verify(view).displayState = DisplayState.FAILURE
            verify(view).errorReason = ErrorReason.TECHNICAL_ERROR
            verify(view).itemList = emptyList()
        }
    }

    @Test
    fun selectBookItem_shouldNavigateToArticleView_whenSuccessful() {
        val catalogueInteractor: CatalogueInteractor = mock {
            on(it.setSelectedBook(any())).thenReturn(Right(Unit))
        }
        val navigation: Navigation = mock()
        val viewModel = CatalogueViewModel(navigation, catalogueInteractor, mock())

        runBlocking { viewModel.selectBook("bookId") }

        inOrder(catalogueInteractor, navigation) {
            verify(catalogueInteractor).setSelectedBook("bookId")
            verify(navigation).goToSelectedArticleView()
        }
    }

    @Test
    fun selectBookItem_shouldDoNothing_whenFailed() {
        val catalogueInteractor: CatalogueInteractor = mock {
            on(it.setSelectedBook(any())).thenReturn(Left(CoreError(Technical)))
        }
        val navigation: Navigation = mock()
        val viewModel = CatalogueViewModel(navigation, catalogueInteractor, mock())

        runBlocking { viewModel.selectBook("bookId") }

        verify(catalogueInteractor).setSelectedBook("bookId")
        verify(navigation, never()).goToSelectedArticleView()
    }
}