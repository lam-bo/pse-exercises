package com.lamboapps.bookshop.app.view.main

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test

class MainViewModelTest {

    @Test
    fun setListeners_shouldSetThemAll() {
        val viewModel = MainViewModel(mock(), mock())
        viewModel.setListeners()
        verify(viewModel.view).onTabSelected = any()
    }

    @Test
    fun selectTab_shouldDoNothing_whenSameTab() {
        val viewModel = MainViewModel(mock(), mock {
            on(it.selectedTab).thenReturn(0)
        })
        viewModel.selectTab(0)
        verify(viewModel.view, never()).selectedTab = any()
    }

    @Test
    fun selectTab_shouldSetSelectedTab_whenDifferentTab() {
        val viewModel = MainViewModel(mock(), mock {
            on(it.selectedTab).thenReturn(0)
        })
        viewModel.selectTab(1)
        verify(viewModel.view).selectedTab = 1
    }
}