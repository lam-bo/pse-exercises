package com.lamboapps.bookshop.app.view.detail

import arrow.core.Right
import com.lamboapps.bookshop.app.view.Navigation
import com.lamboapps.bookshop.core.interactor.DetailInteractor
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.Wishes
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

class DetailViewModelTest {

    val book: Book = Book("id", "title", "synopsis", "coverUrl", 1000)

    @Test
    fun setListeners_shouldSetThemAll() {
        val viewModel = DetailViewModel(mock(), mock(), mock())

        viewModel.setListeners()

        verify(viewModel.view).onAddItemClick = any()
        verify(viewModel.view).onRemoveItemClick = any()
        verify(viewModel.view).onBackNavigationClick = any()
    }

    @Test
    fun initView_shouldLoadSelectedBook_whenNotInBasket() {
        val detailInteractor: DetailInteractor = mock {
            on(it.getCurrentWishes()).thenReturn(Right(Wishes(book, emptySet())))
        }
        val view: Detail.View = mock()
        val viewModel = DetailViewModel(mock(), detailInteractor, view)

        runBlocking { viewModel.initView() }

        argumentCaptor<ItemView>().apply {
            verify(view).itemView = capture()
            assertEquals("title", firstValue.title)
            assertEquals("synopsis", firstValue.synopsis)
            assertEquals("coverUrl", firstValue.coverUrl)
            assertEquals(1000, firstValue.priceInCents)
            assertEquals(false, firstValue.isInBasket)
        }
    }

    @Test
    fun initView_shouldLoadSelectedBook_whenAlreadyInBasket() {
        val detailInteractor: DetailInteractor = mock {
            on(it.getCurrentWishes()).thenReturn(Right(Wishes(book, setOf(book))))
        }
        val view: Detail.View = mock()
        val viewModel = DetailViewModel(mock(), detailInteractor, view)

        runBlocking { viewModel.initView() }

        argumentCaptor<ItemView>().apply {
            verify(view).itemView = capture()
            assertEquals("title", firstValue.title)
            assertEquals("synopsis", firstValue.synopsis)
            assertEquals("coverUrl", firstValue.coverUrl)
            assertEquals(1000, firstValue.priceInCents)
            assertEquals(true, firstValue.isInBasket)
        }
    }

    @Test
    fun navigateBack_shouldCallNavigation() {
        val navigation: Navigation = mock()
        val viewModel: DetailViewModel = DetailViewModel(navigation, mock(), mock())

        viewModel.navigateBack()

        verify(navigation).goBackToMainView(false)
    }

    @Test
    fun addItemToBasket_shouldCallInteractorAndNavigateBack() {
        val navigation: Navigation = mock()
        val interactor: DetailInteractor = mock()
        val viewModel = DetailViewModel(navigation, interactor, mock())

        runBlocking { viewModel.addItemToBasket() }

        inOrder(interactor, navigation) {
            verify(interactor).addSelectedBookToBasket()
            verify(navigation).goBackToMainView(true)
        }
    }

    @Test
    fun removeItemFromBasket_shouldCallInteractorAndNavigateBack() {
        val navigation: Navigation = mock()
        val interactor: DetailInteractor = mock()
        val viewModel = DetailViewModel(navigation, interactor, mock())

        runBlocking { viewModel.removeItemFromBasket() }

        inOrder(interactor, navigation) {
            verify(interactor).removeSelectedBookFromBasket()
            verify(navigation).goBackToMainView(true)
        }
    }

}