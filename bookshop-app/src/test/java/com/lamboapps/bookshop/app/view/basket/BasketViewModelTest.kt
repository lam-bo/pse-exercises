package com.lamboapps.bookshop.app.view.basket

import arrow.core.Left
import arrow.core.Right
import com.lamboapps.bookshop.core.interactor.BasketInteractor
import com.lamboapps.bookshop.core.interactor.BasketInteractor.BasketWithPrices
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Network
import com.lamboapps.bookshop.core.model.Technical
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.junit.Test

class BasketViewModelTest {

    private val basket =
        BasketWithPrices(setOf(Book("isbn", "title", "synopsis", "coverUrl", 1000)), 1000, 200, 800)
    private val emptyBasket = BasketWithPrices(emptySet(), 0, 0, 0)

    @Test
    fun setListeners_shouldSetThemAll() {
        val viewModel = BasketViewModel(mock(), mock(), mock())

        viewModel.setListeners()

        verify(viewModel.view).onPaymentButtonClick = any()
        verify(viewModel.view).onReloadButtonClick = any()
        verify(viewModel.view).onBasketItemClick = any()
    }

    @Test
    fun reloadBasketWithPrices_shouldLoadAndDisplaySuccess_whenNotEmptyItemsList() {
        val basketInteractor: BasketInteractor = mock {
            on(it.getBasketWithPrices()).thenReturn(Right(basket))
        }
        val view: Basket.View = mock()
        val viewModel = BasketViewModel(mock(), basketInteractor, view)

        runBlocking { viewModel.reloadBasketWithPrices() }

        inOrder(view, basketInteractor) {
            verify(view).displayState = DisplayState.LOADING
            verify(basketInteractor).getBasketWithPrices()
            verify(view).displayState = DisplayState.SUCCESS
            verify(view).itemList = any()
            verify(view).basketPrices = any()
        }
    }

    @Test
    fun reloadBasketWithPrices_shouldLoadAndDisplayNoBasket_whenEmptyItemsList() {
        val basketInteractor: BasketInteractor = mock {
            on(it.getBasketWithPrices()).thenReturn(Right(emptyBasket))
        }
        val view: Basket.View = mock()
        val viewModel = BasketViewModel(mock(), basketInteractor, view)

        runBlocking { viewModel.reloadBasketWithPrices() }

        inOrder(view, basketInteractor) {
            verify(view).displayState = DisplayState.LOADING
            verify(basketInteractor).getBasketWithPrices()
            verify(view).displayState = DisplayState.FAILURE
            verify(view).errorReason = ErrorReason.NO_ITEMS
        }
    }

    @Test
    fun reloadBasketWithPrices_shouldLoadAndDisplayFailure_whenNetworkError() {
        val basketInteractor: BasketInteractor = mock {
            on(it.getBasketWithPrices()).thenReturn(Left(CoreError(Network)))
        }
        val view: Basket.View = mock()
        val viewModel = BasketViewModel(mock(), basketInteractor, view)

        runBlocking { viewModel.reloadBasketWithPrices() }

        inOrder(view, basketInteractor) {
            verify(view).displayState = DisplayState.LOADING
            verify(basketInteractor).getBasketWithPrices()
            verify(view).displayState = DisplayState.FAILURE
            verify(view).errorReason = ErrorReason.NETWORK_ERROR
        }
    }

    @Test
    fun reloadBasketWithPrices_shouldLoadAndDisplayFailure_whenTechnicalError() {
        val basketInteractor: BasketInteractor = mock {
            on(it.getBasketWithPrices()).thenReturn(Left(CoreError(Technical)))
        }
        val view: Basket.View = mock()
        val viewModel = BasketViewModel(mock(), basketInteractor, view)

        runBlocking { viewModel.reloadBasketWithPrices() }

        inOrder(view, basketInteractor) {
            verify(view).displayState = DisplayState.LOADING
            verify(basketInteractor).getBasketWithPrices()
            verify(view).displayState = DisplayState.FAILURE
            verify(view).errorReason = ErrorReason.TECHNICAL_ERROR
        }
    }
}