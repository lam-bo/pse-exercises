package com.lamboapps.bookshop.app.repository

import arrow.core.Left
import arrow.core.Right
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.CoreError
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.assertEquals
import org.junit.Test


class BookRepositoryTest {

    private val bookJsonList: List<BooksRemoteSource.BookJsonModel> by lazy {
        val bookJson = BooksRemoteSource.BookJsonModel()
        bookJson.isbn = "isbn"
        bookJson.cover = "cover"
        bookJson.title = "title"
        bookJson.price = 10
        bookJson.synopsis = listOf("synopsis")
        listOf(bookJson)
    }

    private val expectedBookList = listOf(Book("isbn", "title", "synopsis", "cover", 1000))


    @Test
    fun getBookList_shouldSucceed_whenAlreadyNonEmptyLocalList() {
        val bookListRemoteSource: BooksRemoteSource = mock()
        val bookRepository =
            BookRepository(bookListRemoteSource)
        bookRepository.cachedBooks = expectedBookList

        val result = bookRepository.getAllBooks()

        assertEquals(Right(expectedBookList), result)
        verify(bookListRemoteSource, never()).getBooks()
    }

    @Test
    fun getBookList_shouldSucceed_whenNonEmptyRemoteList() {
        val bookRepository =
            BookRepository(mock {
                on(it.getBooks()).doReturn(Right(bookJsonList))
            })

        val result = bookRepository.getAllBooks()

        assertEquals(Right(expectedBookList), result)
    }

    @Test
    fun getBookList_shouldReturnEmptyList_whenNoCompleteBookInRemoteList() {
        val bookRepository =
            BookRepository(mock {
                on(it.getBooks()).doReturn(Right(listOf(BooksRemoteSource.BookJsonModel())))
            })

        val result = bookRepository.getAllBooks()

        assertEquals(Right(emptyList<Book>()), result)
    }

    @Test
    fun getBookList_shouldFail_whenRemoteListFails() {
        val expected = mock<CoreError>()
        val bookRepository =
            BookRepository(mock {
                on(it.getBooks()).doReturn(Left(expected))
            })

        val result = bookRepository.getAllBooks()

        assertEquals(Left(expected), result)
    }
}