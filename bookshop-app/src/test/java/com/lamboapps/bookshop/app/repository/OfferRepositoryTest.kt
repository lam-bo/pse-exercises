package com.lamboapps.bookshop.app.repository

import arrow.core.Left
import arrow.core.Right
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Offer
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class OfferRepositoryTest {

    @Test
    fun getAllOffers_shouldReturnEmpty_whenEmptyIsbnSet() {
        val offerRepository = OfferRepository(mock())

        val result = offerRepository.getAllOffers(emptySet())

        assertEquals(Right(emptyList<OffersRemoteSource.OfferJson>()), result)
    }

    @Test
    fun getAllOffers_shouldSucceed_whenAlreadyCachedOffers() {
        val offerRepository = OfferRepository(mock())
        offerRepository.cachedIsbnSet = setOf("1", "2")
        val expected = mock<Iterable<Offer>>()
        offerRepository.cachedOfferList = expected

        val result = offerRepository.getAllOffers(setOf("1", "2"))

        assertEquals(Right(expected), result)
    }

    @Test
    fun getAllOffers_shouldSucceed_whenRemoteSourceSucceed() {
        val offerRepository = OfferRepository(mock {
            on(it.getOffers(any())).thenReturn(Right(listOf(OffersRemoteSource.OfferJson().apply {
                this.type = "minus"
                this.value = 2
            })))
        })

        val result = offerRepository.getAllOffers(setOf("1", "2"))

        assertEquals(Right(listOf(Offer(Offer.Type.MINUS, 2, null))), result)
    }

    @Test
    fun getAllOffers_shouldFail_whenRemoteSourceFail() {
        val expected = Left(mock<CoreError>())
        val offerRepository = OfferRepository(mock {
            on(it.getOffers(any())).thenReturn(expected)
        })

        val result = offerRepository.getAllOffers(setOf("1", "2"))

        assertEquals(expected, result)
    }
}