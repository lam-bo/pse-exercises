package com.lamboapps.bookshop.app.repository

import arrow.core.Right
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.Wishes
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class WishesRepositoryTest {

    @Test
    fun setSelectedBook() {
        val selectedBook = mock<Book>()
        val repository = WishesRepository()

        val result = repository.setSelectedBook(selectedBook)

        assertEquals(Right(Unit), result)
        assertEquals(selectedBook, repository.selectedBook)
    }

    @Test
    fun resetSelectedBook() {
        val selectedBook = mock<Book>()
        val repository = WishesRepository().apply { this.selectedBook = selectedBook }

        val result = repository.resetSelectedBook()

        assertEquals(Right(Unit), result)
        assertEquals(null, repository.selectedBook)
    }

    @Test
    fun getCurrentWishes() {
        val selectedBook = mock<Book>()
        val repository = WishesRepository().apply {
            this.selectedBook = selectedBook
            this.books.add(selectedBook)
        }

        val result = repository.getCurrentWishes()

        assertEquals(Right(Wishes(selectedBook, setOf(selectedBook))), result)
    }

    @Test
    fun addBookToBasket() {
        val selectedBook = mock<Book>()
        val repository = WishesRepository()

        val result = repository.addBookToBasket(selectedBook)

        assertEquals(Right(Unit), result)
        assertEquals(mutableSetOf(selectedBook), repository.books)
    }

    @Test
    fun removeBookFromBasket() {
        val selectedBook = mock<Book>()
        val repository = WishesRepository().apply { this.books.add(selectedBook) }

        val result = repository.removeBookFromBasket(selectedBook)

        assertEquals(Right(Unit), result)
        assertEquals(emptySet<Book>(), repository.books)
    }
}