Exercises PSE
=============

App Front
=========

L'app front est portée par les modules bookshop-app et bookshop-core.

Architecture MVVM avec comme découpage View <-> View Model <-> Interactor <-> Repository

Les Views sont réalisées avec la librairie expérimentale Jetpack Compose mais isolées le plus possible
pour limiter la casse lors des futures évolutions. Tests interface en WIP dans androidTest.
Utiliser android studio >= Android Studio 4.1 Canary pour bénéficier des previews

Les View Models héritent du view model proposé par Android pour gérer le cycle de vie de l'activité.
Testés unitairement.

Les Interactors contenant la logique métier se trouvent dans le module pur kotlin bookshop-core.
Testés unitairement.

Les Repository sont les sources de données locales ou distantes, testées ou testées partiellement selon
l'implémentation.

Les apk de debug sont récupérables à ce [lien](https://gitlab.com/lam-bo/pse-exercises/pipelines)
et les screenshots sont visibles à ce [lien](https://gitlab.com/lam-bo/pse-exercises/-/tree/master/bookshop-app/screenshots)

App Back
========

Le code relatif à l'algorithme de déplacement des tondeuses se trouve dans le module pur kotlin
lawnmower-core. La version actuelle fait l'hypothèse que plusieurs tondeuses déployées ne peuvent pas occuper
le même endroit au même instant, un déplacement sur une case occupée est ignoré.

[InstructionsInteractor](https://gitlab.com/lam-bo/pse-exercises/-/blob/master/lawnmower-core/src/main/java/com/lamboapps/lawnmower/core/InstructionsInteractor.kt)
est le point de départ de l'algorithme.

Le premier test sur le fichier
[InstructionsInteractorTest](https://gitlab.com/lam-bo/pse-exercises/-/blob/master/lawnmower-core/src/test/java/com/lamboapps/lawnmower/core/InstructionsInteractorTest.kt)
permet de charger le jeu de test fourni, les tests sont exécutés via gitlab-ci.

Le package model contient les différents concepts nécessaires à l'algorithme.
