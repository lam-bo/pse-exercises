package com.lamboapps.bookshop.core.interactor

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Technical
import com.lamboapps.bookshop.core.model.Wishes
import com.nhaarman.mockitokotlin2.mock
import junit.framework.Assert.assertEquals
import org.junit.Test


class DetailInteractorTest {

    private val book: Book = Book("id", "title", "synopsis", "coverUrl", 1000)

    @Test
    fun getCurrentWishes_shouldCallRepository() {
        val expected: Either<CoreError, Wishes> = mock()
        val detailInteractor = DetailInteractor(mock {
            on(it.getCurrentWishes()).thenReturn(expected)
        })

        val result = detailInteractor.getCurrentWishes()

        assertEquals(expected, result)
    }

    @Test
    fun addSelectedItemToBasket_shouldCallRepository_whenSelectedItemNotNull() {
        val expected: Either<CoreError, Unit> = Right(Unit)
        val detailInteractor = DetailInteractor(mock {
            on(it.getCurrentWishes()).thenReturn(Right(Wishes(book, emptySet())))
            on(it.addBookToBasket(book)).thenReturn(expected)
        })

        val result = detailInteractor.addSelectedBookToBasket()

        assertEquals(expected, result)
    }

    @Test
    fun addSelectedItemToBasket_shouldFail_whenSelectedItemNull() {
        val expected: Either<CoreError, Unit> = Left(CoreError(Technical))
        val detailInteractor = DetailInteractor(mock {
            on(it.getCurrentWishes()).thenReturn(Right(Wishes(null, emptySet())))
        })

        val result = detailInteractor.addSelectedBookToBasket()

        assertEquals(expected, result)
    }

    @Test
    fun removeSelectedItemFromBasket_shouldCallRepository_whenSelectedItemNotNull() {
        val expected: Either<CoreError, Unit> = Right(Unit)
        val detailInteractor = DetailInteractor(mock {
            on(it.getCurrentWishes()).thenReturn(Right(Wishes(book, emptySet())))
            on(it.removeBookFromBasket(book)).thenReturn(expected)
        })

        val result = detailInteractor.removeSelectedBookFromBasket()

        assertEquals(expected, result)
    }

    @Test
    fun removeSelectedItemFromBasket_shouldFail_whenSelectedItemNull() {
        val expected: Either<CoreError, Unit> = Left(CoreError(Technical))
        val detailInteractor = DetailInteractor(mock {
            on(it.getCurrentWishes()).thenReturn(Right(Wishes(null, emptySet())))
        })

        val result = detailInteractor.removeSelectedBookFromBasket()

        assertEquals(expected, result)
    }
}