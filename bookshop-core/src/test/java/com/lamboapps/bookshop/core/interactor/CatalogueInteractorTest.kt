package com.lamboapps.bookshop.core.interactor

import arrow.core.Left
import arrow.core.Right
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Wishes
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class CatalogueInteractorTest {

    private val book: Book = Book("id", "title", "synopsis", "coverUrl", 1000)

    @Test
    fun getBookList_shouldReturnBookList_whenSuccessful() {
        val expected = Right(listOf(mock<Book>()))
        val catalogueInteractor = CatalogueInteractor(mock {
            on(it.getAllBooks()).thenReturn(expected)
        }, mock())

        catalogueInteractor.getBookList()

        assertEquals(expected, catalogueInteractor.getBookList())
    }

    @Test
    fun getBookList_shoulFail_whenRepositoryBookListFails() {
        val expected = Left(mock<CoreError>())
        val catalogueInteractor = CatalogueInteractor(mock {
            on(it.getAllBooks()).thenReturn(expected)
        }, mock())

        catalogueInteractor.getBookList()

        assertEquals(expected, catalogueInteractor.getBookList())
    }

    @Test
    fun setSelectedBook_shoulCallRepository() {
        val wishesRepository: Wishes.Repository = mock {
            on(it.setSelectedBook(book)).thenReturn(Right(Unit))
        }
        val catalogueInteractor = CatalogueInteractor(mock {
            on(it.getAllBooks()).thenReturn(Right(listOf(book)))
        }, wishesRepository)

        val result = catalogueInteractor.setSelectedBook("id")

        assertEquals(Right(Unit), result)
    }
}