package com.lamboapps.bookshop.core.interactor

import arrow.core.Left
import arrow.core.Right
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Offer
import com.lamboapps.bookshop.core.model.Wishes
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class BasketInteractorTest {

    private val book: Book = Book("id", "title", "synopsis", "coverUrl", 1000)
    private val offers: List<Offer> =
        listOf(Offer(Offer.Type.MINUS, 3, null), Offer(Offer.Type.MINUS, 5, null))

    @Test
    fun getBasketWithPrices_shouldFail_whenWishesRepositoryFails() {
        val expected = Left(mock<CoreError>())
        val basketInteractor = BasketInteractor(mock {
            on(it.getCurrentWishes()).thenReturn(expected)
        }, mock())

        val result = basketInteractor.getBasketWithPrices()

        assertEquals(expected, result)
    }

    @Test
    fun getBasketWithPrices_shouldFail_whenOfferRepositoryFails() {
        val expected = Left(mock<CoreError>())
        val basketInteractor = BasketInteractor(mock {
            on(it.getCurrentWishes()).thenReturn(Right(Wishes(book, setOf(book))))
        }, mock {
            on(it.getAllOffers(any())).thenReturn(expected)
        })

        val result = basketInteractor.getBasketWithPrices()

        assertEquals(expected, result)
    }

    @Test
    fun getBasketWithPrices_shouldReturnBestOffer() {
        val expected = Right(BasketInteractor.BasketWithPrices(setOf(book), 1000, 500, 500))
        val basketInteractor = BasketInteractor(mock {
            on(it.getCurrentWishes()).thenReturn(Right(Wishes(book, setOf(book))))
        }, mock {
            on(it.getAllOffers(any())).thenReturn(Right(offers))
        })

        val result = basketInteractor.getBasketWithPrices()

        assertEquals(expected, result)
    }
}