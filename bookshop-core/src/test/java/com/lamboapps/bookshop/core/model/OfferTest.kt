package com.lamboapps.bookshop.core.model

import org.junit.Assert.assertEquals
import org.junit.Test

class OfferTest {

    @Test
    fun priceReductionInCents_shouldReturnMinusPrice_whenMinusType() {
        val offer = Offer(Offer.Type.MINUS, 10, null)

        val priceReductionInCents = offer.priceReductionInCents(2000)

        assertEquals(1000, priceReductionInCents)
    }

    @Test
    fun priceReductionInCents_shouldReturnPercentPrice_whenPercentType() {
        val offer = Offer(Offer.Type.PERCENTAGE, 10, null)

        val priceReductionInCents = offer.priceReductionInCents(10000)

        assertEquals(1000, priceReductionInCents)
    }

    @Test
    fun priceReductionInCents_shouldReturnSlicePrice_whenSliceType() {
        val offer = Offer(Offer.Type.SLICE, 10, 100)

        val priceReductionInCents = offer.priceReductionInCents(20000)

        assertEquals(2000, priceReductionInCents)
    }

    @Test
    fun priceReductionInCents_shouldReturnZero_whenSliceValueNull() {
        val offer = Offer(Offer.Type.SLICE, 10, null)

        val priceReductionInCents = offer.priceReductionInCents(20000000)

        assertEquals(0, priceReductionInCents)
    }

    @Test
    fun priceReductionInCents_shouldReturnZero_whenBasePriceZero() {
        val offer = Offer(Offer.Type.MINUS, 10, null)

        val priceReductionInCents = offer.priceReductionInCents(0)

        assertEquals(0, priceReductionInCents)
    }

    @Test
    fun priceReductionInCents_shouldReturnBasePrice_whenCalculatedReductionAboveBasePrice() {
        val offer = Offer(Offer.Type.MINUS, 999999999, null)

        val priceReductionInCents = offer.priceReductionInCents(1000)

        assertEquals(1000, priceReductionInCents)
    }
}