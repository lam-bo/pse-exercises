package com.lamboapps.bookshop.core.interactor

import arrow.core.Either
import arrow.core.Left
import arrow.core.flatMap
import com.lamboapps.bookshop.core.model.*

class BasketInteractor(
    private val wishesRepository: Wishes.Repository,
    private val offerRepository: Offer.Repository
) {

    data class BasketWithPrices(
        val books: Set<Book>,
        val basePriceInCents: Int,
        val offerPriceInCents: Int,
        val fullPriceInCents: Int
    )

    fun getBasketWithPrices(): Either<CoreError, BasketWithPrices> {
        return wishesRepository.getCurrentWishes()
            .map { it.basketSet }
            .flatMap { basket ->
                val basePrice = basket.sumBy { it.priceInCents }.coerceAtLeast(0)
                val isbnList = basket.map { it.isbn }.toSet()
                offerRepository.getAllOffers(isbnList).map { offers ->
                    val bestOffer = offers.maxBy { it.priceReductionInCents(basePrice) }
                    val offerPrice = bestOffer?.priceReductionInCents(basePrice) ?: 0
                    val fullPrice = basePrice - offerPrice
                    BasketWithPrices(basket, basePrice, offerPrice, fullPrice)
                }
            }
    }

    fun setSelectedBook(bookId: String): Either<CoreError, Unit> {
        return getBasketWithPrices().map { basket -> basket.books.find { it.isbn == bookId } }
            .flatMap {
                if (it != null) {
                    wishesRepository.setSelectedBook(it)
                } else {
                    Left(CoreError(Technical))
                }
            }
    }
}
