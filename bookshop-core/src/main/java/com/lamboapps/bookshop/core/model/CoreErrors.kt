package com.lamboapps.bookshop.core.model

data class CoreError(val marker: CoreErrorMarker, val logData: Map<String, String> = emptyMap())

sealed class CoreErrorMarker
object Technical : CoreErrorMarker()
object Network : CoreErrorMarker()