package com.lamboapps.bookshop.core.model

import arrow.core.Either

data class Offer(
    val type: Type,
    val value: Int,
    val sliceValue: Int?
) {
    enum class Type { PERCENTAGE, MINUS, SLICE }

    interface Repository {
        fun getAllOffers(isbnSet: Set<String>): Either<CoreError, Iterable<Offer>>
    }

    fun priceReductionInCents(basePriceInCents: Int): Int {
        return with(basePriceInCents.coerceAtLeast(0)) {
            when (type) {
                Type.MINUS -> value * 100
                Type.PERCENTAGE -> this * value / 100
                Type.SLICE -> {
                    val sliceValueInCents = sliceValue?.coerceAtLeast(1)?.times(100)
                    this / (sliceValueInCents ?: Int.MAX_VALUE) * value * 100
                }
            }.coerceIn(0, this)
        }
    }
}