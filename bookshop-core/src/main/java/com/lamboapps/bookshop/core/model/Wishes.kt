package com.lamboapps.bookshop.core.model

import arrow.core.Either

data class Wishes(
    val selectedBook: Book?,
    val basketSet: Set<Book>
) {

    fun isSelectedBookInBasket(): Boolean? {
        return selectedBook?.let { basketSet.contains(it) }
    }

    interface Repository {
        fun setSelectedBook(book: Book): Either<CoreError, Unit>
        fun resetSelectedBook(): Either<CoreError, Unit>
        fun getCurrentWishes(): Either<CoreError, Wishes>
        fun addBookToBasket(book: Book): Either<CoreError, Unit>
        fun removeBookFromBasket(book: Book): Either<CoreError, Unit>
    }
}