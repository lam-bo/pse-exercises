package com.lamboapps.bookshop.core.model

import arrow.core.Either

data class Book(
    val isbn: String,
    val title: String,
    val synopsis: String,
    val coverUrl: String,
    val priceInCents: Int
) {

    interface Repository {
        fun getAllBooks(): Either<CoreError, Iterable<Book>>
    }
}