package com.lamboapps.bookshop.core.interactor

import arrow.core.Either
import arrow.core.Left
import arrow.core.flatMap
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Technical
import com.lamboapps.bookshop.core.model.Wishes


class DetailInteractor(private val wishesRepository: Wishes.Repository) {

    fun getCurrentWishes(): Either<CoreError, Wishes> {
        return wishesRepository.getCurrentWishes()
    }

    fun addSelectedBookToBasket(): Either<CoreError, Unit> {
        return wishesRepository.getCurrentWishes().flatMap { wishes ->
            if (wishes.selectedBook != null) {
                wishesRepository.addBookToBasket(wishes.selectedBook)
            } else {
                Left(CoreError(Technical))
            }
        }
    }

    fun removeSelectedBookFromBasket(): Either<CoreError, Unit> {
        return wishesRepository.getCurrentWishes().flatMap { wishes ->
            if (wishes.selectedBook != null) {
                wishesRepository.removeBookFromBasket(wishes.selectedBook)
            } else {
                Left(CoreError(Technical))
            }
        }
    }
}