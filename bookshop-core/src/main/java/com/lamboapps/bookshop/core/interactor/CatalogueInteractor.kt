package com.lamboapps.bookshop.core.interactor

import arrow.core.Either
import arrow.core.Left
import arrow.core.flatMap
import com.lamboapps.bookshop.core.model.Book
import com.lamboapps.bookshop.core.model.CoreError
import com.lamboapps.bookshop.core.model.Technical
import com.lamboapps.bookshop.core.model.Wishes

class CatalogueInteractor(
    private val bookRepository: Book.Repository,
    private val wishesRepository: Wishes.Repository
) {

    fun getBookList(): Either<CoreError, List<Book>> {
        return bookRepository.getAllBooks().map { it.toList() }
    }

    fun setSelectedBook(bookId: String): Either<CoreError, Unit> {
        return bookRepository.getAllBooks()
            .map { books -> books.find { it.isbn == bookId } }
            .flatMap {
                if (it != null) {
                    wishesRepository.setSelectedBook(it)
                } else {
                    Left(CoreError(Technical))
                }
            }
    }
}